import { useEffect, useMemo, useState, useRef } from "react";
import Result from "./Result";
import SpeechRecognition, {
  useSpeechRecognition,
} from "react-speech-recognition";

const synth = window.speechSynthesis;

function App() {
  const { transcript, listening, resetTranscript } = useSpeechRecognition();

  const [text, setText] = useState("");
  const [rate, setRate] = useState(1);
  const timeoutId = useRef(null);

  useEffect(() => {
    setText(transcript);

    if (timeoutId.current) clearTimeout(timeoutId.current);

    timeoutId.current = setTimeout(() => {
      SpeechRecognition.stopListening();
    }, 2000);
  }, [transcript]);

  const voices = useMemo(() => synth.getVoices(), []);
  const [voiceSelected, setVoiceSelected] = useState("Google US English");

  const [isSpeaking, setIsSpeaking] = useState("");
  const [meanings, setMeanings] = useState([]);
  const [phonetics, setPhonetics] = useState([]);
  const [word, setWord] = useState("");
  const [error, setError] = useState("");

  const startSpeech = (text) => {
    const utterance = new SpeechSynthesisUtterance(text);
    const voice = voices.find((voice) => voice.name === voiceSelected);
    utterance.voice = voice;
    utterance.rate = rate;
    synth.speak(utterance);
  };

  const dictionaryApi = (text) => {
    let url = `https://api.dictionaryapi.dev/api/v2/entries/en/${text}`;
    fetch(url)
      .then((res) => res.json())
      .then((result) => {
        setMeanings(result[0].meanings);
        setPhonetics(result[0].phonetics);
        setWord(result[0].word);
        setError("");
      })
      .catch((error) => setError(error));
  };

  const reset = () => {
    setIsSpeaking("");
    setError("");
    setMeanings([]);
    setPhonetics([]);
    setWord("");
  };

  useEffect(() => {
    if (!text.trim()) return reset();
    const debounce = setTimeout(() => {
      dictionaryApi(text);
    }, 800);
    return () => clearTimeout(debounce);
  }, [text]);

  const handleSpeech = () => {
    if (!text.trim()) return;
    if (!synth.speaking) {
      startSpeech(text);
      setIsSpeaking("speaking");
    } else {
      synth.cancel();
    }

    setInterval(() => {
      if (!synth.speaking) {
        setIsSpeaking("");
      }
    }, 100);
  };

  return (
    <div className="App">
      <div className="container">
        <h1>English Dictionary</h1>

        <form>
          <div className="row">
            <textarea
              value={text}
              onChange={(e) => setText(e.target.value)}
              rows="12"
              placeholder="Enter text"
            />
            <div className="voices-icons">
              <div className="select-voices">
                <select
                  value={voiceSelected}
                  onChange={(e) => setVoiceSelected(e.target.value)}
                >
                  {voices.map((voice) => (
                    <option key={voice.name}>{voice.name}</option>
                  ))}
                </select>
              </div>
              <i class="fa-duotone fa-waveform-lines"></i>
              <input
                style={{ marginLeft: "1rem" }}
                type="range"
                min={0.5}
                max={2}
                value={rate}
                step={0.1}
                onChange={(e) => setRate(e.target.value)}
              />

              {!listening && (
                <i
                  className="fa-sharp fa-solid fa-microphone-slash"
                  onClick={() => {
                    SpeechRecognition.startListening({ continuous: true });
                    resetTranscript("");
                  }}
                ></i>
              )}
              {/* voice icon when listening */}
              {listening && (
                <div
                  className="icon"
                  onClick={() => SpeechRecognition.stopListening()}
                >
                  <svg
                    focusable="false"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      fill="#4285f4"
                      d="m12 15c1.66 0 3-1.31 3-2.97v-7.02c0-1.66-1.34-3.01-3-3.01s-3 1.34-3 3.01v7.02c0 1.66 1.34 2.97 3 2.97z"
                    ></path>
                    <path fill="#34a853" d="m11 18.08h2v3.92h-2z"></path>
                    <path
                      fill="#fbbc05"
                      d="m7.05 16.87c-1.27-1.33-2.05-2.83-2.05-4.87h2c0 1.45 0.56 2.42 1.47 3.38v0.32l-1.15 1.18z"
                    ></path>
                    <path
                      fill="#ea4335"
                      d="m12 16.93a4.97 5.25 0 0 1 -3.54 -1.55l-1.41 1.49c1.26 1.34 3.02 2.13 4.95 2.13 3.87 0 6.99-2.92 6.99-7h-1.99c0 2.92-2.24 4.93-5 4.93z"
                    ></path>
                  </svg>
                </div>
              )}

              {/* volume icon */}
              <i
                onClick={handleSpeech}
                className={`fa-solid fa-volume-high ${isSpeaking}`}
              />
            </div>
          </div>
        </form>

        {text.trim() !== "" && !error && (
          <Result
            word={word}
            meanings={meanings}
            phonetics={phonetics}
            setText={setText}
          />
        )}
      </div>
    </div>
  );
}

export default App;
